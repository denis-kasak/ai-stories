---
title: Whispers of Petals
date: 2023-11-16 19:29:45
tags:
- haiku
- poetry
---

{% asset_img haiku.png %}

Statue-child with blooms,
Eyes alight with silent tales,
Red petals whisper.

---

*The text in this post was generated using ChatGPT 4.*
*The image(s) in this posts were generated using Dreamstudio by Stability AI*